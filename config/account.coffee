
# account.coffee
#
# Define all links, personal info, etc.
#

accountSettings =
  name: "Rodolfo Sanchez"
  title: "Software Developer"
  media:
    linkedIn:
      url: "https://www.linkedin.com/pub/rodolfo-sanchez/3a/724/833/"
      icon: "linkedin.svg"
      label: "LinkedIn"
    github:
      url: "https://github.com/rodolfo2488"
      icon: "social-github.svg"
      label: "GitHub"
    blog:
      url: "#"
      icon: "ic_book_48px.svg"
      label: "Blog"

module.exports = accountSettings
