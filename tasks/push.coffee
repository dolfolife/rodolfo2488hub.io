#
# example.coffee:
#   Example of a custom task for Grunt.
#
#   If you want to add a custom task, you may use this as a
#   template.
#

task = (grunt, options) ->

  path        = require "path"
  name        = path.basename(__filename, ".coffee")

  description = "Push to gh-papges"

  job         = ["jade", "gh-pages"]

  grunt.registerTask name, description, job

module.exports = task
