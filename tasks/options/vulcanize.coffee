
#
# vulcanize.coffee:
#

contents = (grunt, options) ->

  settings = require "../../config/settings"
  path     = require "path"

  default:
    options: {}
    files:
      ".dist/index.html": path.join(settings.buildDirectory, 'pre-index.html')


module.exports = contents
