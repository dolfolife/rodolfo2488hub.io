#
#
# copy.coffee:
#   copy files into .dist
#

contents = (grunt, options) ->

  settings = require "../../config/settings"
  path     = require "path"

  main:
    files: [{
        expand: true
        cwd: path.join(settings.appRoot, "img")
        src: [ "**"]
        dest: path.join(settings.buildDirectory, "img")
      }]

module.exports = contents
