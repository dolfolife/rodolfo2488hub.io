#
# watch.coffee:
#

contents = (grunt, options) ->

  settings = require "../../config/settings"
  path     = require "path"

  jade:
    files: path.join(settings.appRoot, "**", "*")
    tasks: ["build"]
    options:
      livereload: true


module.exports = contents
