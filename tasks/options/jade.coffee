
#
#
# example.coffee:
#   Example of a option file for a task in Grunt.
#
#   If you want to set options for a task, duplicate this file,
#   with name equal to the task to be configured and change
#   the contents variable.
#

contents = (grunt, options) ->

  settings = require "../../config/settings"
  path     = require "path"

  compile:
    options:
      data:
        debug: false
    files:
      ".dist/pre-index.html": [path.join(settings.appRoot, "index.jade")]

module.exports = contents
