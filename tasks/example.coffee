#
# example.coffee:
#   Example of a custom task for Grunt.
#
#   If you want to add a custom task, you may use this as a
#   template.
#

task = (grunt, options) ->

  settings    = require "../config/settings"

  # Automatic task naming using the name of the script.
  path        = require "path"
  name        = path.basename(__filename, ".coffee")

  # Description of the task for grunt help.

  description = "Example task"

  # The job performed by this task. Could be a function,
  # a list of tasks, etc...

  job         = ->
    grunt.log.writeln (grunt.config.get "example.message")
  
  grunt.registerTask name, description, job

module.exports = task
