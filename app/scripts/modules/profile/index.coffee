'use strict'

controller = require "./controller.coffee"
template = require "./templates/index.jade"

module.exports = angular.module('rodolfo2488.profile', [])
.config( ($stateProvider, $urlRouterProvider) ->

  $urlRouterProvider.otherwise("/account")
  $stateProvider
   .state('account',
        url: "/account"
        template: template,
        controller: controller
      )
)
